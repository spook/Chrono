#ifndef KEYPAD_H__
#define KEYPAD_H__

#define USE_KEYPAD

#define KEY_RIGHT  0
#define KEY_UP     1
#define KEY_DOWN   2
#define KEY_LEFT   3
#define KEY_SELECT 4
#define KEY_NONE   5

#define KEY_ANY_THRESHOLD 1000
#define KEY_RIGHT_THRESHOLD 50
#define KEY_UP_THRESHOLD 250
#define KEY_DOWN_THRESHOLD 450 
#define KEY_LEFT_THRESHOLD 650
#define KEY_SELECT_THRESHOLD 850

#define LAST_READ_DELAY1 250
#define LAST_READ_DELAY2 125
#define LAST_READ_DELAY3 50
#define REPEAT_COUNT1 5
#define REPEAT_COUNT2 20

#define keypadPort 0

  class Keypad
  {
    private:
      unsigned long lastRead;
      unsigned char lastKey;
      unsigned char repeats;

      void setLastRead(int key);
    public:
      Keypad();
      unsigned char ReadKey();
  };

#endif
