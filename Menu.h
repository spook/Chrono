#ifndef MENU_H__
#define MENU_H__

#include <stdio.h>
#include <stdlib.h>
#include "Display.h"
#include "Arduino.h"

// For simplicity, these values match ones in Keypad
#define MENU_ACTION_RIGHT 0
#define MENU_ACTION_UP 1
#define MENU_ACTION_DOWN 2
#define MENU_ACTION_LEFT 3
#define MENU_ACTION_SELECT 4

#define TEXT_BACK "< Back"
#define TEXT_BACK_LEN 6

#ifndef SELECTION_CHAR
#define SELECTION_CHAR '>'
#endif

#define MENU_TYPE_INT 0
#define MENU_TYPE_BOOL 1
#define MENU_TYPE_SUB 2
#define MENU_TYPE_ACTION 3

#ifndef MAX_MENU_LEVELS
#define MAX_MENU_LEVELS 5
#endif

typedef void (*IntValueChangedCallback)(int newValue);
typedef void (*BoolValueChangedCallback)(bool newValue);

// Forward declarations -------------------------------------------------------

class MainMenu;

// BaseMenuItem ---------------------------------------------------------------

class BaseMenuItem
{
protected:
  const char * name;
  unsigned char len;

public:
  BaseMenuItem(const char * name);
  void ShowListItem(Display & display, int row, bool selected);
  const char * GetName();
  virtual int GetType() = 0;
};

// MenuItem -------------------------------------------------------------------

class MenuItem : public BaseMenuItem
{
friend class MainMenu;
  
private:
  void ShowEditMode(Display & display);
  
protected:
  void ShowEditHeader(Display & display);
  virtual void ShowEditValue(Display & display) = 0;
  virtual void HandleAction(int key) = 0;  
  virtual void StartEdit() = 0;

public:
  MenuItem(const char * name);  
};

// IntMenuItem ----------------------------------------------------------------

class IntMenuItem : public MenuItem
{
friend class MainMenu;
  
private:
  int min;
  int max;
  IntValueChangedCallback callback;
  
protected:
  int value;
  void ShowEditValue(Display & display);
  void HandleAction(int key);
  void StartEdit();

public:
  IntMenuItem(const char * name, int min, int max, int defaultValue);
  IntMenuItem(const char * name, int min, int max, int defaultValue, IntValueChangedCallback callback);
  int GetType();
};

// TimeMenuItem ---------------------------------------------------------------

class TimeMenuItem : public IntMenuItem
{
protected:
  void ShowEditValue(Display & display);

public:
  TimeMenuItem(const char * name, int min, int max, int defaultValue);
  TimeMenuItem(const char * name, int min, int max, int defaultValue, IntValueChangedCallback callback);
};

// BoolMenuItem ---------------------------------------------------------------

class BoolMenuItem : public MenuItem
{
friend class MainMenu;
  
private:
  bool value;
  BoolValueChangedCallback callback;
  
protected:
  void ShowEditValue(Display & display);
  void HandleAction(int key);
  void StartEdit();
  
public:
  BoolMenuItem(const char * name, bool defaultValue);
  BoolMenuItem(const char * name, bool defaultValue, BoolValueChangedCallback callback);
  int GetType();
};

// SubMenuItem ----------------------------------------------------------------

class SubMenuItem : public BaseMenuItem
{
friend class MainMenu;
  
private:
  BaseMenuItem ** items;
  int count;

public:
  SubMenuItem(const char * name, BaseMenuItem ** items, int count);
  int GetCount();
  BaseMenuItem * GetItem(int index);
  int GetType();
};

// ActionMenuItem -------------------------------------------------------------

class ActionMenuItem : public BaseMenuItem
{
private: 
  int actionId;

public:
  ActionMenuItem(const char * name, int actionId);
  int GetActionId();
  int GetType();
};

// MainMenu -------------------------------------------------------------------

class MainMenu : public SubMenuItem
{
private:
  SubMenuItem ** itemStack; 
  short * positionStack;
  unsigned char stackTop;
  short selectedItem;
  short firstItem;
  Display & display;
  MenuItem * editedItem;

  void ShowBack(int row, bool isSelected);
  
public:
  MainMenu(Display & display, BaseMenuItem ** items, int count);
  ~MainMenu();
  int Action(int keyAction);
  void Show();
};

#endif
